CastCompanionLibrary
===

This is library is mandatory for Brightcove Cast Plugin Android forked from [here](https://github.com/googlecast/CastCompanionLibrary-android).

Clone this library into a project named CastCompanionLibrary, parallel to your own application project.
Brightcove Cast Plugin's Companion Library 
